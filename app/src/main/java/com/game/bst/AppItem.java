package com.game.bst;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by La Costra on 01/12/2015.
 */
public class AppItem {
    public static boolean a = false;
    public static String b = "myPrefs";
    public static String c;
    public static String d;
    public static boolean e;
    public static String f = "stored_credits";
    public static ArrayList g;
    public static SharedPreferences h;
    public static SharedPreferences.Editor i;
    private String j;
    private String k;
    private String l;
    private int m;
    private String n;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    private Drawable icon;

    public static boolean isA() {
        return a;
    }

    public static void setA(boolean a) {
        AppItem.a = a;
    }

    public static void e(String var0) {
//        if(a) {
//            Log.d("GameBooster Custom Log", var0);
//        }

    }

    public final String a() {
        return this.j;
    }

    public final void a(int var1) {
        this.m = var1;
    }

    public final void a(String var1) {
        this.j = var1;
    }

    public final String b() {
        return this.k;
    }

    public final void b(String var1) {
        this.k = var1;
    }

    public final void c(String var1) {
        this.l = var1;
    }

    public final void d(String var1) {
        this.n = var1;
    }
}
