package com.game.bst;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;



@SuppressWarnings("ResourceType")
public final class AppsAdapter extends BaseAdapter {
   private LayoutInflater a;
   private List list;
   private Map c;
   private Drawable d;
   private Context e;

   public AppsAdapter(Context var1) {
      this.a = LayoutInflater.from(var1);
      this.e = var1;
      this.d = var1.getResources().getDrawable(R.drawable.ic_launcher);
   }

   public final void a(List var1) {
      this.list = var1;
   }

   public final void a(Map var1) {
      this.c = var1;
   }

   public final int getCount() {
      return this.list.size();
   }

   public final Object getItem(int var1) {
      return this.list.get(var1);
   }

   public final long getItemId(int var1) {
      return (long)var1;
   }

   public final View getView(final int var1, View var2, ViewGroup var3) {
      //ViewHolder var4 = (ViewHolder)this.list.get(var1);
      boolean var5;
//      if(var4.b().contains("ad.ad.ad.ad")) {
//         var5 = true;
//      } else {
//         var5 = false;
//      }
      AppItem item= (AppItem) getItem(var1);
      ViewHolder var6;
      if(var2 == null) {
         var2 = this.a.inflate(R.layout.row, var3, false);
         var6 = new ViewHolder();
         var6.title = (TextView)var2.findViewById(R.id.apptitle);
         var6.icon = (ImageView)var2.findViewById(R.id.appicon);
         //var6.d = (Button)var2.findViewById(R.id.btn_install);
         var6.c = (TextView)var2.findViewById(R.id.ad_disclaimer);
         var2.setTag(var6);
      } else {
         var6 = (ViewHolder)var2.getTag();
      }


         var6.icon.setImageDrawable(item.getIcon());
      var6.title.setText(item.a());

         var6.c.setVisibility(8);


      return var2;
   }

   public final class ViewHolder {
      TextView title;
      ImageView icon;
      TextView c;
      Button d;

      public final void a(Drawable var1) {
         if(var1 != null) {
            this.icon.setImageDrawable(var1);
         }
      }

      public final void setTitle(String var1) {
         this.title.setText(var1);
      }
   }
}
