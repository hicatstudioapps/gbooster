package com.game.bst;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Created by La Costra on 30/11/2015.
 */
public class Widget extends AppWidgetProvider  {
    public void onUpdate(Context var1, AppWidgetManager var2, int[] var3) {

    for(int var6 = 0; var6 < var3.length; ++var6) {
        int var7 = var3[var6];
        try {
            Intent var4 = new Intent("android.intent.action.MAIN");
            var4.addCategory("android.intent.category.LAUNCHER");
            var4.addFlags(65536);
            var4.setComponent(new ComponentName("com.game.bst", "com.game.bst.MainActivity"));
            PendingIntent var5 = PendingIntent.getActivity(var1, 0, var4, 0);
            RemoteViews var9 = new RemoteViews(var1.getPackageName(), R.layout.widget_layout);
            var9.setOnClickPendingIntent(R.id.imageView1, var5);
            var2.updateAppWidget(var7, var9);
        } catch (ActivityNotFoundException var8) {
            var8.printStackTrace();
        }
    }
}
}
