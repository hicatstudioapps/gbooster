package com.game.bst;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by La Costra on 30/11/2015.
 */
public class AppList extends AppCompatActivity implements AdapterView.OnItemClickListener{
    int a = 0;
    int b = 0;
    private AppsAdapter d;
    private List e;
    private ListView listView;
    private ProgressDialog progress;

    // $FF: synthetic method
    static List a(AppList var0) {
        ArrayList var2 = new ArrayList();
        PackageManager var4 = var0.getPackageManager();
        Iterator var3 = var4.getInstalledPackages(0).iterator();

        while(var3.hasNext()) {
            PackageInfo var1 = (PackageInfo)var3.next();
            if((var1.applicationInfo.flags & 1) != 1 && !var1.packageName.equals(var0.getApplicationContext().getPackageName())) {
                AppItem var5 = new AppItem();
                var5.a(var1.applicationInfo.loadLabel(var4).toString());
                var5.b(var1.packageName);
                var5.c(var1.versionName);
                var5.a(var1.versionCode);
                var5.setIcon(var1.applicationInfo.loadIcon(var4));
                CharSequence var6 = var1.applicationInfo.loadDescription(var4);
                String var7;
                if(var6 != null) {
                    var7 = var6.toString();
                } else {
                    var7 = "";
                }
                var5.d(var7);
                var2.add(var5);
            }
        }
        return var2;
    }

     public void onCreate(Bundle var1) {
        super.onCreate(var1);
       // this.requestWindowFeature(1);
        this.setContentView(R.layout.main);
        this.listView = (ListView)this.findViewById(R.id.appslist);
        this.listView.setOnItemClickListener(this);
        this.progress = new ProgressDialog(this);
        this.progress.setMessage(getString(R.string.load_apps));
        this.progress.setIndeterminate(true);
        this.progress.setCancelable(false);
        this.d = new AppsAdapter(this.getApplicationContext());
        new AppList.a().execute();
    }

    public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
        AppItem var6 = (AppItem)var1.getItemAtPosition(var3);
        Intent var7 = new Intent();
        var7.putExtra("package", var6.b());
        this.setResult(-1, var7);
        this.finish();
    }

    private class a extends AsyncTask<Void,Void,List<AppItem>> {

        private List a() {
 return  AppList.a(AppList.this);
        }


        protected List<AppItem> doInBackground(Void... var1) {
            return this.a();
        }

        @Override
        protected void onPostExecute(List<AppItem> appItems) {
            super.onPostExecute(appItems);
            List var2 = (List)appItems;
            AppList.this.d.a(var2);
            AppList.this.listView.setAdapter(AppList.this.d);
            AppList.this.d.notifyDataSetChanged();
            AppList.this.progress.dismiss();
        }

        public final void onPreExecute() {
            AppList.this.progress.show();
        }
    }
}
