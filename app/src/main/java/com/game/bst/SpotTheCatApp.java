package com.game.bst;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class SpotTheCatApp extends Application {


    public static  InterstitialAd interstitialAd;
    AdRequest request;
    public static Typeface face;
    public static Typeface faceR;
    public static SharedPreferences preferences;



   public void onCreate() {
      super.onCreate();
//      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
//      ImageLoader.getInstance().init(config);
      interstitialAd=new InterstitialAd(this);
      interstitialAd.setAdUnitId("ca-app-pub-1147824671772507/2203873678");
       request= new AdRequest.Builder().build();
      interstitialAd.setAdListener(new AdListener() {
          @Override
          public void onAdClosed() {
              interstitialAd.loadAd(request);
          }

      });
      interstitialAd.loadAd(request);
//        if(getSharedPreferences("data",MODE_PRIVATE).getBoolean(Constants.CONTENT_READY,false))
      face= Typeface.createFromAsset(getAssets(),"f.otf");
      // faceR= Typeface.createFromAsset(getAssets(),"R.OTF");

   }

		public static void showInterstitial(){
			if(interstitialAd.isLoaded()){
				interstitialAd.show();
			}
		}
}
