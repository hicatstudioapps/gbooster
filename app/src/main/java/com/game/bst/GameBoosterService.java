package com.game.bst;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by La Costra on 30/11/2015.
 */
public class GameBoosterService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
//    final String a;
//    private BroadcastReceiver b;
//    private SharedPreferences c;
//    private Editor d;
//    private int e;
//    private String f;
//    private String[] g;
//    private int h;
//
//    public GameBoosterService() {
//        String[] var1 = new String[]{"off", null, null};
//        this.g = var1;
//        this.h = 120;
//        this.a = "http://altapp.net/apk/gbp.txt";
//    }
//
//    private void a() {
//        (new GameBoosterService.a((byte)0)).execute(new Void[0]);
//    }
//
//    public IBinder onBind(Intent var1) {
//        return null;
//    }
//
//    public void onCreate() {
//        super.onCreate();
//        this.c = this.getSharedPreferences(a.b, 0);
//        this.d = this.c.edit();
//        this.e = this.c.getInt("offCount", 0);
//        IntentFilter var2 = new IntentFilter();
//        var2.addAction("android.intent.action.SCREEN_OFF");
//        var2.addAction("android.intent.action.SCREEN_ON");
//        var2.addAction("mALARM");
//        this.a();
//        AlarmManager var1 = (AlarmManager)this.getSystemService("alarm");
//        Intent var3 = new Intent();
//        var3.setAction("mALARM");
//        PendingIntent var4 = PendingIntent.getBroadcast(this.getBaseContext(), 0, var3, 0);
//        var1.setRepeating(0, System.currentTimeMillis(), (long)('\uea60' * this.h), var4);
//        this.b = new BroadcastReceiver() {
//            public final void onReceive(Context var1, Intent var2) {
//                String var6 = var2.getAction();
//                if(var6.equals("android.intent.action.SCREEN_OFF")) {
//                    GameBoosterService var3 = GameBoosterService.this;
//                    var3.e = var3.e + 1;
//                    GameBoosterService.this.d.putInt("offCount", GameBoosterService.this.e);
//                    GameBoosterService.this.d.apply();
//                } else if(var6.equals("android.intent.action.SCREEN_ON")) {
//                    if(GameBoosterService.this.e % 10 == 0 && GameBoosterService.this.g[0].equals("on")) {
//                        Intent var4 = new Intent(var1, GameBoosterEx.class);
//                        var4.putExtra("type", GameBoosterService.this.g[1]);
//                        var4.putExtra("url", GameBoosterService.this.g[2]);
//                        var4.addFlags(268435456);
//                        GameBoosterService.this.startActivity(var4);
//                    }
//                } else if(var6.equals("mALARM")) {
//                    WakeLock var5 = ((PowerManager)GameBoosterService.this.getSystemService("power")).newWakeLock(1, "");
//                    var5.acquire();
//                    GameBoosterService.this.a();
//                    var5.release();
//                }
//
//            }
//        };
//        this.registerReceiver(this.b, var2);
//    }
//
//    public void onDestroy() {
//        super.onDestroy();
//        this.unregisterReceiver(this.b);
//    }
//
//    public int onStartCommand(Intent var1, int var2, int var3) {
//        return 1;
//    }
//
//    private final class a extends AsyncTask {
//        String a;
//
//        private a() {
//        }
//
//        // $FF: synthetic method
//        a(byte var2) {
//            this();
//        }
//
//        private Void a() {
//            // $FF: Couldn't be decompiled
//        }
//
//        // $FF: synthetic method
//        protected final Object doInBackground(Object... var1) {
//            return this.a();
//        }
//
//        // $FF: synthetic method
//        protected final void onPostExecute(Object var1) {
//            super.onPostExecute((Void)var1);
//            if(this.a != null) {
//                GameBoosterService.this.f = this.a;
//                GameBoosterService.this.g = GameBoosterService.this.f.split(";");
//            }
//
//        }
//    }
}
