package com.game.bst;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


/**
 * Created by La Costra on 30/11/2015.
 */
@SuppressWarnings("ResourceType")
public class MainActivity extends AppCompatActivity {


    TextView getGames;
    TextView textView;
    LinearLayout linearLayout;
    boolean g = true;
    boolean h = false;
    private ImageButton imageButton;
    private ImageButton[] imageButtons;
    private MainActivity mainActivity;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private boolean o = false;
    private boolean p = false;
    ProgressDialog var2;
    private AdView adView;


    static void a(MainActivity var0, int var1) {
        var0.startActivityForResult(new Intent(var0.mainActivity, AppList.class), var1);
    }


    private void reloadInfo() {
        this.memoryInfo();
        this.getProccess();
    }

    private void runAppDialog(final String var1) {
        AlertDialog.Builder var2 = new AlertDialog.Builder(this.mainActivity);
        var2.setTitle(this.getString(R.string.bookmarks_dialog));
        var2.setMessage(this.getString(R.string.run_app_dialog));
        var2.setPositiveButton(this.getString(0x104000a), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface var1x, int var2) {
                optimizingDialog(MainActivity.this, var1);
            }
        });
        var2.setNegativeButton(this.getString(17039360), (DialogInterface.OnClickListener)null);
        var2.create().show();
    }



    static void removeBookMarkDialog(final MainActivity var0, final int var1) {
        AlertDialog.Builder var2 = new AlertDialog.Builder(var0.mainActivity);
        var2.setTitle(var0.getString(R.string.bookmarks_dialog));
        var2.setMessage(var0.getString(R.string.remove_bm));
        var2.setPositiveButton(var0.getString(0x104000a), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface var1x, int var2) {
                var0.editor.putString("bm" + var1, (String) null);
                var0.editor.apply();
                var0.fillMarker();
            }
        });
        var2.setNegativeButton(var0.getString(17039360), (DialogInterface.OnClickListener) null);
        var2.create().show();
    }

    // $FF: synthetic method
    void optimizingDialog(final MainActivity var0, final String var1) {
        new LaunchApp(var1).execute();
    }

    private void memoryInfo() {
        ActivityManager.MemoryInfo var1 = new ActivityManager.MemoryInfo();
        ((ActivityManager)this.getSystemService("activity")).getMemoryInfo(var1);
        long var2 = var1.availMem / 1048576L;
        ((TextView)this.findViewById(R.id.memory)).setText(String.valueOf(var2) + "MB");
    }

    private void getProccess() {
        TextView var1 = (TextView)this.findViewById(R.id.processes);
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP){
        ActivityManager var2 = (ActivityManager)this.mainActivity.getSystemService("activity");

        int var4 = 0;

        Iterator var5 = var2.getRunningAppProcesses().iterator();
        while(true) {
            ActivityManager.RunningAppProcessInfo var3;
            do {
                if(!var5.hasNext()) {
                    return;
                }
                var3 = (ActivityManager.RunningAppProcessInfo)var5.next();
            } while(var3.processName.equals("system_process") && var3.processName.equals(this.mainActivity.getPackageName()));
            ++var4;
            var1.setText("" + var4);
        }}else{
          List<ProcessManager.Process> list= ProcessManager.getRunningApps();
            var1.setText(list.size()+"");

        }
    }

    private void fillMarker() {
        for(int var2 = 0; var2 < this.imageButtons.length; ++var2) {
            String var1 = this.sharedPreferences.getString("bm" + var2, (String)null);
            if(var1 != null) {
                try {
                    Drawable var4 = this.getPackageManager().getApplicationIcon(var1);
                    this.imageButtons[var2].setBackgroundDrawable(var4);
                } catch (PackageManager.NameNotFoundException var3) {
                    var3.printStackTrace();
                    sharedPreferences.edit().putString("bm"+var2,(String)null).commit();
                }
            } else {
                this.imageButtons[var2].setBackgroundResource(R.drawable.add_app_btn);
            }
        }

    }

    private void f() {
        ActivityManager var1 = (ActivityManager)this.mainActivity.getSystemService("activity");
        if(Build.VERSION.SDK_INT< Build.VERSION_CODES.LOLLIPOP) {
            ListIterator var2 = var1.getRunningAppProcesses().listIterator();
            while (var2.hasNext()) {
                ActivityManager.RunningAppProcessInfo var3 = (ActivityManager.RunningAppProcessInfo) var2.next();
                if (!var3.processName.equals("system_process") && !var3.processName.equals(this.mainActivity.getPackageName()))
                    var1.killBackgroundProcesses(var3.processName);
            }
        }else{
            List<ProcessManager.Process> list= ProcessManager.getRunningApps();
            for (ProcessManager.Process p:
            list){
                var1.killBackgroundProcesses(p.getPackageName());
            }
        }
//        for ( ActivityManager.RunningAppProcessInfo temp : var2) {
//            if(temp.processName.equals("system_process") && temp.processName.equals(this.mainActivity.getPackageName()))
//                continue;
//        }
//        while(true) {
//            ActivityManager.RunningAppProcessInfo var3;
//            do {
//                if(!var2.hasNext()) {
//                    this.reloadInfo();
//                    return;
//                }
//
//                 = (ActivityManager.RunningAppProcessInfo)var2.next();
//            } while();
//
//            var1.killBackgroundProcesses(var3.processName);
//           // a.e("Pausing process " + var3.processName);
//        }
    }

    private boolean isPro() {
        return true;
    }

    // $FF: synthetic method
    static void i(MainActivity var0) {
        var0.o = true;
    }

    public void RefreshInfo(View var1) {
        this.reloadInfo();
    }

    public void SettingsGear(View var1) {
        this.openOptionsMenu();
    }

    public void ShowHelp(View var1) {
        this.openOptionsMenu();
    }

    public void onActivityResult(final int var1, int var2, Intent var3) {
        super.onActivityResult(var1, var2, var3);
        if(var2 == -1) {
            Bundle var5 = var3.getExtras();
            if(var5 == null) {
              //  a.textView("extras is null");
            } else {
                final String var6 = var5.getString("package");
                //a.textView("package selected is " + var6);
                if(var1 == 23) {
                    this.runAppDialog(var6);
                } else {
                    AlertDialog.Builder var4 = new AlertDialog.Builder(this.mainActivity);
                    var4.setTitle(this.getString(R.string.bookmarks_dialog));
                    var4.setMessage(this.getString(R.string.add_app_bm));
                    var4.setPositiveButton(this.getString(0x104000a), new DialogInterface.OnClickListener() {
                        public final void onClick(DialogInterface var1x, int var2) {
                            MainActivity.this.editor.putString("bm" + var1, var6);
                            MainActivity.this.editor.apply();
                            MainActivity.this.fillMarker();
                        }
                    });
                    var4.setNegativeButton(this.getString(0x1040000), new DialogInterface.OnClickListener() {
                        public final void onClick(DialogInterface var1, int var2) {
                        }
                    });
                    var4.create().show();
                }
            }
        }

    }

    public void onBackPressed() {

            this.finish();
//        } else {
//            this.a.onBackPressed();
//            this.p = true;
//        }

    }

    protected void onCreate(Bundle var1) {
        super.onCreate(var1);
//        this.requestWindowFeature(1);
//        ServerUtilities.registerWithGCM(this);
        this.setContentView(R.layout.activity_main);
        this.adView = (AdView)this.findViewById(R.id.adView);
        AdRequest var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });
        //this.setRequestedOrientation(1);
        android.support.v7.widget.Toolbar t= ( android.support.v7.widget.Toolbar)findViewById(R.id.toolbar);
        t.setTitle("");
        t.setNavigationIcon(getResources().getDrawable(R.drawable.nav_icon));
        setSupportActionBar(t);
        t.setOverflowIcon(getResources().getDrawable(R.drawable.gear));
        //t.setVisibility(View.GONE);
        ((TextView)findViewById(R.id.bmTitle)).setTypeface(SpotTheCatApp.face);
        ((TextView)findViewById(R.id.sysInfo)).setTypeface(SpotTheCatApp.face);
        ((TextView)findViewById(R.id.tagmem)).setTypeface(SpotTheCatApp.face);
        ((TextView)findViewById(R.id.tagproc)).setTypeface(SpotTheCatApp.face);
        ((TextView)findViewById(R.id.memory)).setTypeface(SpotTheCatApp.face);
        ((TextView)findViewById(R.id.processes)).setTypeface(SpotTheCatApp.face);
        var2 = new ProgressDialog(this);
        this.mainActivity = this;
        this.sharedPreferences = this.getSharedPreferences("pref", 0);
        this.editor = this.sharedPreferences.edit();
        this.imageButtons = new ImageButton[]{(ImageButton)this.findViewById(R.id.bm1), (ImageButton)this.findViewById(R.id.bm2), (ImageButton)this.findViewById(R.id.bm3), (ImageButton)this.findViewById(R.id.bm4)};
        for(int var2 = 0; var2 < this.imageButtons.length; ++var2) {
            final int position=var2;
            this.imageButtons[var2].setOnClickListener(new android.view.View.OnClickListener() {
                public final void onClick(View var1) {
                    MainActivity var2x = MainActivity.this;
                 //   a.textView("CLICK - Image view " + var2);
                    String var3 = MainActivity.this.sharedPreferences.getString("bm" + position, (String)null);
                    if(var3 == null) {
                        MainActivity.a(MainActivity.this, position);
                    } else {
                        MainActivity.this.runAppDialog(var3);
                    }

                }
            });
            this.imageButtons[var2].setOnLongClickListener(new View.OnLongClickListener() {
                public final boolean onLongClick(View var1) {
                    MainActivity var2x = MainActivity.this;
                   // a.textView("LONG CLICK - Image view " + var2);
                    if(MainActivity.this.sharedPreferences.getString("bm" + position, (String)null) == null) {
                        MainActivity.a(MainActivity.this, position);
                    } else {
                        MainActivity.removeBookMarkDialog(MainActivity.this, position);
                    }

                    return false;
                }
            });
        }

        this.fillMarker();
        this.imageButton = (ImageButton)this.findViewById(R.id.launchButton);
        this.imageButton.setOnClickListener(new android.view.View.OnClickListener() {
            public final void onClick(View var1) {
                Intent var2 = new Intent(MainActivity.this.mainActivity, AppList.class);
                MainActivity.this.startActivityForResult(var2, 23);
            }
        });
        findViewById(R.id.imageView1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder var2 = new AlertDialog.Builder(MainActivity.this);
                var2.setMessage(getString(R.string.help));
                var2.setNeutralButton(getString(0x104000a), (DialogInterface.OnClickListener)null);
                var2.create().show();
            }
        });
        if(this.sharedPreferences.getBoolean("firt", false)) {
           if(!this.sharedPreferences.getBoolean("hasRated", false)) {
                AlertDialog.Builder var11 = new AlertDialog.Builder(this.mainActivity);
                var11.setTitle(this.getString(R.string.please_rate));
                var11.setMessage(this.getString(R.string.please_rate_2));
                var11.setPositiveButton(this.getString(R.string.rate_5_stars), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface var1, int var2) {
//                        AppRater.rateNow(MainActivity.this);
                        MainActivity.this.editor.putBoolean("hasRated", true);
                        MainActivity.this.editor.apply();
                    }
                });
                var11.setNegativeButton(this.getString(0x1040000), (DialogInterface.OnClickListener)null);
                var11.create().show();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu var1) {
        getMenuInflater().inflate(R.menu.main, var1);
return true;
    }

    protected void onDestroy() {
        super.onDestroy();

    }

    public boolean onOptionsItemSelected(MenuItem var1) {
        int var3 = var1.getItemId();
        switch (var3){
            case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String text = String.format(getString(R.string.share_app_default_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.help:
                AlertDialog.Builder var2 = new AlertDialog.Builder(this.mainActivity);
                var2.setMessage(this.getText(R.string.help));
                var2.setNeutralButton(this.getString(0x104000a), (DialogInterface.OnClickListener)null);
                var2.create().show();
                break;
            case R.id.rate:
//                AppRater.rateNow(this);
                this.editor.putBoolean("hasRated", true);
                this.editor.apply();
                break;
            case R.id.memory:
                f();
                reloadInfo();
                SpotTheCatApp.showInterstitial();
                break;
        }

        return super.onOptionsItemSelected(var1);
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
            this.memoryInfo();
            this.getProccess();
           sharedPreferences.edit().putBoolean("firt",true).commit();
    }


    class LaunchApp extends AsyncTask<Void,Void,Void>{
        private String pckName;

        public LaunchApp(String pckName) {
            this.pckName = pckName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            var2.setCancelable(false);
            var2.setMessage(getString(R.string.pd_message));
            var2.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            var2.dismiss();
            reloadInfo();
            Intent intent = getPackageManager().getLaunchIntentForPackage(pckName);
            startActivity(intent);
            SpotTheCatApp.showInterstitial();
        }

        @Override
        protected Void doInBackground(Void... params) {
            f();
            return null;
        }
    }
}
